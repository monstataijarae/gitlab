# To contribute improvements to CI/CD templates, please follow the Development guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html
# This specific template is located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Dependency-Scanning.gitlab-ci.yml

# Read more about this feature here: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/
#
# Configure dependency scanning with CI/CD variables (https://docs.gitlab.com/ee/ci/variables/index.html).
# List of available variables: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/index.html#available-variables

variables:
  # Setting this variable will affect all Security templates
  # (SAST, Dependency Scanning, ...)
  SECURE_ANALYZERS_PREFIX: "registry.gitlab.com/security-products"
  DS_EXCLUDED_ANALYZERS: ""
  DS_EXCLUDED_PATHS: "spec, test, tests, tmp"
  DS_MAJOR_VERSION: 3

dependency_scanning:
  stage: test
  script:
    - echo "$CI_JOB_NAME is used for configuration only, and its script should not be executed"
    - exit 1
  artifacts:
    reports:
      dependency_scanning: gl-dependency-scanning-report.json
  dependencies: []
  rules:
    - when: never

.ds-analyzer:
  extends: dependency_scanning
  allow_failure: true
  variables:
    # DS_ANALYZER_IMAGE is an undocumented variable used internally to allow QA to
    # override the analyzer image with a custom value. This may be subject to change or
    # breakage across GitLab releases.
    DS_ANALYZER_IMAGE: "$SECURE_ANALYZERS_PREFIX/$DS_ANALYZER_NAME:$DS_MAJOR_VERSION"
    # DS_ANALYZER_NAME is an undocumented variable used in job definitions
    # to inject the analyzer name in the image name.
    DS_ANALYZER_NAME: ""
  image:
    name: "$DS_ANALYZER_IMAGE$DS_IMAGE_SUFFIX"
  # `rules` must be overridden explicitly by each child job
  # see https://gitlab.com/gitlab-org/gitlab/-/issues/218444
  script:
    - /analyzer run

.cyclone-dx-reports:
  artifacts:
    paths:
      - "**/cyclonedx-*.json"

gemnasium-dependency_scanning:
  extends:
    - .ds-analyzer
    - .cyclone-dx-reports
  variables:
    DS_ANALYZER_NAME: "gemnasium"
    GEMNASIUM_LIBRARY_SCAN_ENABLED: "true"
  rules:
    - if: $DEPENDENCY_SCANNING_DISABLED
      when: never
    - if: $DS_EXCLUDED_ANALYZERS =~ /gemnasium([^-]|$)/
      when: never
    - if: $CI_COMMIT_BRANCH &&
          $GITLAB_FEATURES =~ /\bdependency_scanning\b/
      exists:
        - '{Gemfile.lock,*/Gemfile.lock,*/*/Gemfile.lock}'
        - '{composer.lock,*/composer.lock,*/*/composer.lock}'
        - '{gems.locked,*/gems.locked,*/*/gems.locked}'
        - '{go.sum,*/go.sum,*/*/go.sum}'
        - '{npm-shrinkwrap.json,*/npm-shrinkwrap.json,*/*/npm-shrinkwrap.json}'
        - '{package-lock.json,*/package-lock.json,*/*/package-lock.json}'
        - '{yarn.lock,*/yarn.lock,*/*/yarn.lock}'
        - '{packages.lock.json,*/packages.lock.json,*/*/packages.lock.json}'
        - '{conan.lock,*/conan.lock,*/*/conan.lock}'

gemnasium-maven-dependency_scanning:
  extends:
    - .ds-analyzer
    - .cyclone-dx-reports
  variables:
    DS_ANALYZER_NAME: "gemnasium-maven"
  rules:
    - if: $DEPENDENCY_SCANNING_DISABLED
      when: never
    - if: $DS_EXCLUDED_ANALYZERS =~ /gemnasium-maven/
      when: never
    - if: $CI_COMMIT_BRANCH &&
          $GITLAB_FEATURES =~ /\bdependency_scanning\b/
      exists:
        - '{build.gradle,*/build.gradle,*/*/build.gradle}'
        - '{build.gradle.kts,*/build.gradle.kts,*/*/build.gradle.kts}'
        - '{build.sbt,*/build.sbt,*/*/build.sbt}'
        - '{pom.xml,*/pom.xml,*/*/pom.xml}'

gemnasium-python-dependency_scanning:
  extends:
    - .ds-analyzer
    - .cyclone-dx-reports
  variables:
    DS_ANALYZER_NAME: "gemnasium-python"
  rules:
    - if: $DEPENDENCY_SCANNING_DISABLED
      when: never
    - if: $DS_EXCLUDED_ANALYZERS =~ /gemnasium-python/
      when: never
    - if: $CI_COMMIT_BRANCH &&
          $GITLAB_FEATURES =~ /\bdependency_scanning\b/
      exists:
        - '{requirements.txt,*/requirements.txt,*/*/requirements.txt}'
        - '{requirements.pip,*/requirements.pip,*/*/requirements.pip}'
        - '{Pipfile,*/Pipfile,*/*/Pipfile}'
        - '{requires.txt,*/requires.txt,*/*/requires.txt}'
        - '{setup.py,*/setup.py,*/*/setup.py}'
        # Support passing of $PIP_REQUIREMENTS_FILE
        # See https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#configuring-specific-analyzers-used-by-dependency-scanning
    - if: $CI_COMMIT_BRANCH &&
          $GITLAB_FEATURES =~ /\bdependency_scanning\b/ &&
          $PIP_REQUIREMENTS_FILE
